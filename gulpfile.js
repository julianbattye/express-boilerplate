var gulp = require('gulp');
var browserSync = require('browser-sync').create();

var browserSyncConfig = {
	// 1. inform browser-sync to watch compiled *.css files instead of *.scss sass files
	files: ["app/public/css/**/*.css", "app/views/*.pug" ],
	// 2. Tell BrowserSync where to proxy to the app
	proxy: "localhost:3000",
	// Don't show any notifications in the browser.
	notify: false
}

// browser-sync task starts the server
gulp.task('browser-sync', function() {
	browserSync.init(browserSyncConfig);
});

gulp.task('default', ['browser-sync']);
